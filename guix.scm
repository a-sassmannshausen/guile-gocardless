(use-modules
  (guix packages)
  (guix licenses)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "guile-gocardless")
  (version "0.1")
  (source "./guile-gocardless-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-2.2)))
  (propagated-inputs `(("guile-json" ,guile-json)))
  (synopsis
    "Library to interact with the GoCardless payment provider API")
  (description
    "Guile GoCardless provides high-level bindings to the GoCardless API.  At this point, most end-points are provided, though some are still missing.  The implementation is relatively naive, though it implements rate-limiting and is configurable using parameters.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/guile-gocardless")
  (license gpl3+))

