;; gocardless/web.scm --- web implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2018 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-gocardless.
;;
;; guile-gocardless is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-gocardless is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-gocardless; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (gocardless web)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (json)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:use-module (web http)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web client)
  #:export (access-token
            base-host
            debug?

            rsp->assoc 

            invalidate-cache

            post-headers put-headers

            gocardless-get gocardless-post gocardless-put))

(define access-token (make-parameter "Bearer token-here"))
(define base-host
  (make-parameter (build-uri 'https #:host "api-sandbox.gocardless.com")))
(define debug? (make-parameter #f))

(declare-opaque-header! "GoCardless-Version")
(declare-header! "Authorization"
                 (lambda (str)
                   (match (string-split str #\space)
                     ((verb token)
                      (cons (string->symbol (string-downcase verb))
                            token))
                     (_ (throw 'auth-header
                               "STR should be a string of two words!"))))
                 (match-lambda
                   (('bearer . (? string?)) #t)
                   (_ #f))
                 (lambda (header port)
                   (match header
                     (('bearer . (? string? str))
                      (display (string-append "Bearer " str) port)))))

(define* (base-uri #:optional (path "") (params '()))
  (build-uri (uri-scheme (base-host)) #:host (uri-host (base-host))
             #:path path
             #:query (if (null? params) #f (params->query-string params))))

(define (rsp->assoc scm)
  (let lp ((scm scm))
    (match scm
      (() '())
      ((? hash-table? scm)
       (lp (hash-map->list cons scm)))
      (((? hash-table? scm) ...)
       (map lp (hash-map->list cons scm)))
      ((((? string? name) . (? hash-table? htable)) . rest)
       (cons
        (cons name (lp (hash-map->list cons htable)))
        (lp rest)))
      ((((? string? name) . ((? hash-table? htable) ...)) . rest)
       (cons
        (cons name (map (compose lp (cut hash-map->list cons <>)) htable))
        (lp rest)))
      ((((? string? name) . else) . rest)
       (cons
        (cons name else)
        (lp rest)))
      (_ 'gocardless-rsp->assoc "Unexpected token in SCM" scm))))

(define (params->query-string params)
  ;; No validation, but formats should be:
  ;; created_* : date-time : ${YYYY}-${MM}-${DD}T${HH}:${MM}:${SS}.${mmm}Z
  ;; (e.g. 2017-11-28T00:00:00.000Z)
  ;; limit : integer
  (string-join (filter-map (match-lambda
                             ((name . #f) #f)
                             (("limit" . n)
                              (string-append "limit=" (number->string n)))
                             ((name . value)
                              (string-append name "=" value)))
                           params)
               "&"))

(define* (base-headers token)
  `((authorization . ,(parse-header 'authorization token))
    (connection . ,(parse-header 'connection "close"))
    (gocardless-version . ,(parse-header 'gocardless-version "2015-07-06"))
    (accept . ,(parse-header 'accept "application/json"))))

(define* (post-headers token #:optional idempotency-key)
  (let ((headers (cons `(content-type
                         . ,(parse-header 'content-type "application/json"))
                       (base-headers token))))
    (if idempotency-key
        (cons `(idempotency-key
                . ,(parse-header 'itempotency-key idempotency-key))
              headers)
        headers)))

(define* (put-headers token #:optional idempotency-key)
  (let ((headers (cons `(content-type
                         . ,(parse-header 'content-type "application/json"))
                       (base-headers token))))
    (if idempotency-key
        (cons `(idempotency-key
                . ,(parse-header 'itempotency-key idempotency-key))
              headers)
        headers)))

(define (invalidate-cache)
  (gocardless-get "" #:invalidate-cache? #t))

(define gocardless-get
  (let ((cache (make-hash-table)))
    (lambda* (path #:optional (params '()) failure-map
                   (headers (base-headers (access-token)))
                   #:key invalidate-cache? show-cache?)
      (cond
       (invalidate-cache? (set! cache (make-hash-table)))
       (show-cache? cache)
       (else
        (let ((uri (base-uri path params)))
          (if (debug?)
              (throw 'debug uri headers)
              (match (hash-ref cache (cons (access-token) uri))
                (#f
                 (let ((result (call-with-values
                                   (lambda _ (http-get uri #:headers headers))
                                 (cut response-handler <> <> failure-map))))
                   (hash-set! cache (cons (access-token) uri) result)
                   result))
                (rsp rsp)))))))))

(define* (gocardless-post path #:optional (headers '()) body failure-map)
  (if (debug?)
      (throw 'debug (base-uri path '()) headers body)
      (call-with-values
          (lambda _ (http-post (base-uri path '()) #:headers headers
                               #:body body))
        (cut response-handler <> <> failure-map))))

(define* (gocardless-put path #:optional (headers '()) body failure-map)
  (if (debug?)
      (throw 'debug (base-uri path '()) headers body)
      (call-with-values
          (lambda _ (http-put (base-uri path '()) #:headers headers
                              #:body body))
        (cut response-handler <> <> failure-map))))

;; FIXME: We should handle the core response code defined at
;; https://developer.gocardless.com/api-reference/#api-usage-response-codes
;; After this each request should contain a failure-map that maps specific
;; failures in the response body to that request, to handle per request
;; failure modes.
;; Currently we are only handling a fraction of the GoCardless error codes,
;; and none of the per request failure modes.
(define* (response-handler response body failure-map)
  ;; Check the response is OK. Error otherwise.
  ;; Check rate limiting.  If dangerously low, sleep until reset.
  ;; Return body ready for use.
  (when (< (ratelimit-remaining response) 30)
    (format #t "Waiting ~a sec for ratelimit reset (~a requests remaining).~%"
            (ratelimit-wait (ratelimit-reset response))
            (ratelimit-remaining response))
    (sleep (ratelimit-wait (ratelimit-reset response))))
  (if failure-map
      (throw 'not-implemented)
      (cond ((or (= 200 (response-code response))
                 (= 201 (response-code response)))
             (json-string->scm (utf8->string body)))
            ((= 429 (response-code response))
             (throw 'gocardless-429
                    "Too many requests, despite our attempts at rate-limiting."))
            (else
             (throw 'gocardless-code
                    (response-code response)
                    (rsp->assoc (json-string->scm (utf8->string body))))))))

;;;; Response Header Accessors

(define (ratelimit-limit response)
  (match (assoc 'ratelimit-limit (response-headers response))
    (#f (throw 'gocardless-syntax
               "No ratelimit-limit returned.  This should never happen!"))
    (('ratelimit-limit . count) (string->number count))))

(define (ratelimit-remaining response)
  (match (assoc 'ratelimit-remaining (response-headers response))
    (#f (throw 'gocardless-syntax
               "No ratelimit-remaining returned.  This should never happen!"))
    (('ratelimit-remaining . count) (string->number count))))

(define (ratelimit-wait ratelimit-date)
  "Return the number of seconds we should wait until our ratelimit reset."
  ;; FIXME: This will fail around the the change of day
  ;; (if current time is 23:59:nn), because the ratelimit-date will be for the
  ;; next day.
  (if (equal? (date->string (current-date) "~1")
              (date->string ratelimit-date "~1"))
      (1+ (time-second (time-difference (date->time-utc ratelimit-date)
                                        (current-time))))
      (throw 'gocardless-syntax
             "Date provided by GoCardless is not the same as current date!"
             ratelimit-date)))

(define (ratelimit-reset response)
  (match (assoc 'ratelimit-reset (response-headers response))
    (#f (throw 'gocardless-syntax
               "No ratelimit-reset returned.  This should never happen!"))
    (('ratelimit-reset . date)
     (match (string-split date #\space)
       ;;d  nn XXX   nnnn HH:MM:00 GMT
       ((_ day month year (= (cute string-split <> #\:) time) zone)
        ;; FIXME: We need to manually parse the date because srfi-19's
        ;; date->string is broken.
        (make-date 0 0 (string->number (second time))
                   (string->number (first time))
                   (string->number day)
                   (match month
                     ("Jan" 01)
                     ("Feb" 02)
                     ("Mar" 03)
                     ("Apr" 04)
                     ("May" 05)
                     ("Jun" 06)
                     ("Jul" 07)
                     ("Aug" 08)
                     ("Sep" 09)
                     ("Oct" 10)
                     ("Nov" 11)
                     ("Dec" 12))
                   (string->number year) 0))))))
