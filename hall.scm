(hall-description
 (name "gocardless")
 (prefix "guile")
 (version "0.1")
 (author "Alex Sassmannshausen")
 (copyright (2018))
 (synopsis "Library to interact with the GoCardless payment provider API")
 (description "Guile GoCardless provides high-level bindings to the GoCardless API.  At this point, most end-points are provided, though some are still missing.  The implementation is relatively naive, though it implements rate-limiting and is configurable using parameters.")
 (home-page
  "https://gitlab.com/a-sassmannshausen/guile-gocardless")
 (license gpl3+)
 (dependencies `(("guile-json" ,guile-json)))
 (files (libraries
         ((scheme-file "gocardless")
          (directory "gocardless" ((scheme-file "web")))))
        (tests ((directory
                 "tests"
                 ((scheme-file "gocardless")))))
        (programs ((directory "scripts" ())))
        (documentation
         ((text-file "README")
          (text-file "HACKING")
          (text-file "COPYING")
          (text-file "NEWS")
          (text-file "ChangeLog")
          (text-file "AUTHORS")
          (directory "doc" ((texi-file "gocardless")))))
        (infrastructure
         ((scheme-file "guix") (scheme-file "hall")))))
