(define-module (gocardless)
  #:use-module (gocardless web)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 regex)
  #:use-module (json)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:re-export (access-token base-host debug? invalidate-cache rsp->assoc)
  #:export (access
            gc:map gc:print linked

            cursor-before cursor-after cursor-limit

            date->gc:date date->gc:date-time
            gc:date-time->date gc:date->date

            list-customers get-customer

            list-events get-event

            list-subscriptions get-subscription create-subscription
            update-subscription cancel-subscription

            list-mandates get-mandate reinstate-mandate

            list-payments get-payment

            customers customer-id customer-created_at customer-email
            customer-given_name customer-family_name
            customer-address_line1 customer-address_line2
            customer-address_line3 customer-city customer-region
            customer-postal_code customer-country_code customer-language
            customer-metadata.URN

            events event-id event-details event-detail-description
            event-detail-scheme event-detail-reason_code event-detail-cause
            event-detail-origin event-created_at event-metadata event-action
            event-links event-resource_type

            subscriptions subscription-id subscription-amount
            subscription-created_at subscription-currency subscription-status
            subscription-name subscription-start_date subscription-end_date
            subscription-interval subscription-interval_unit
            subscription-day_of_month subscription-month
            subscription-payment_reference subscription-upcoming_payments
            subscription-metadata subscription-links subscription-links-mandate

            mandates mandate-id mandate-created_at mandate-reference
            mandate-status mandate-scheme mandate-next_possible_charge_date
            mandate-metadata mandate-links mandate-links-customer

            payments payment-id payment-amount payment-amount_refunded
            payment-charge_date payment-created_at payment-currency
            payment-description payment-metadata payment-reference
            payment-status payment-links payment-links-creditor
            payment-links-mandate payment-links-payout
            payment-links-subscription))

;;;; Utilities

(define (gc:date-time->date str)
  (string->date str "~Y~m~d~H~M~S~z"))

(define (gc:date->date str)
  (string->date str "~Y~m~d"))

(define (date->gc:date date)
  "Return a string representing a date as expected by GoCardless' date
fields."
  (date->string date "~1"))

(define (date->gc:date-time date)
  "Return a string representing a date as expected by GoCardless' date-time
fields, with no time zone modifier."
  (date->string date "~5.000Z"))

(define (gc:map pager accessor proc)
  "Iterate over API contents using the paging procedure PAGER
 (e.g. list-subscriptions), which should accept at least the #:after keyword.
We use ACCESSOR to access the contents part of the API response, and apply
PROC to the final list (of lists) of contents."
  (let lp ((current (pager #:limit 500))
           (result '()))
    (let ((content (accessor current)))
      (match (cursor-after current)
        (()
         (proc (concatenate (reverse (cons content result)))))
        (last-id (lp (pager #:limit 500 #:after last-id)
                     (cons content result)))))))

(define (param-list . additional)
  (append '("after" "before" "created_at[gt]"
            "created_at[gte]" "created_at[lt]"
            "created_at[lte]" "limit")
          additional))

(define (args->params params . values)
  (if (= (length params) (length values))
      (map cons params values)
      (throw 'gocardless-args->params "PARAMS & VALUES not same length"
             params values)))

;;;; Requests

;;;;; Customers

(define* (list-customers #:key after before created_at> created_at>=
                         created_at< created_at<= (limit 50))
  (gocardless-get "/customers"
                  (args->params (param-list)
                                after before created_at> created_at>=
                                created_at< created_at<= limit)))

(define (get-customer id)
  (gocardless-get (string-append "/customers/" id)))

;;;;; Events

(define* (list-events #:key action after before created_at> created_at>=
                      created_at< created_at<= (limit 50) include parent_event
                      mandate payment payout refund subscription
                      resource_type)
  (gocardless-get "/events"
                  (args->params (param-list "action" "include" "parent_event"
                                            "mandate" "payment" "payout"
                                            "refund" "subscription"
                                            "resource_type")
                                after before created_at> created_at>=
                                created_at< created_at<= limit action include
                                parent_event mandate payment payout refund
                                subscription resource_type)))

(define (get-event id)
  (gocardless-get (string-append "/events/" id)))

;;;;; Subscriptions

(define* (list-subscriptions #:key after before created_at> created_at>=
                             created_at< created_at<= (limit 50)
                             customer mandate status)
  (gocardless-get "/subscriptions"
                  (args->params (param-list "customer" "mandate" "status")
                                after before created_at> created_at>=
                                created_at< created_at<= limit customer
                                mandate status)))

(define (get-subscription id)
  (gocardless-get (string-append "/subscriptions/" id)))

;; Recurrence Rules
;;
;; The following rules apply when specifying recurrence:
;; - The first payment must be charged within 1 year.
;; - When neither month nor day_of_month are present, the subscription will
;;   recur from the start_date based on the interval_unit.
;; - If month or day_of_month are present, the recurrence rules will be
;;   applied from the start_date, and the following validations apply:
;;   (https://developer.gocardless.com/api-reference/#subscriptions)
(define* (create-subscription amount currency interval_unit mandate
                              idempotency-key #:key app_fee count day_of_month
                              interval metadata month name payment_reference start_date)
  ;; This will update the service: must invalidate cache.
  (invalidate-cache)
  (gocardless-post
   "/subscriptions/"
   (post-headers (access-token) idempotency-key)
   (scm->json-string
    `((subscriptions
       . ((amount . ,amount)                ; in pence | cents | öre
          (currency . ,currency)            ; GBP | EUR | SEK
          (interval_unit . ,interval_unit)  ; weekly | monthly | yearly
          (links . ((mandate . ,mandate)))  ; reference to existing mandate
          ,@(filter (match-lambda
                      ((n . #f) #f)
                      ((n . v) #t))
                    `((app_fee . ,app_fee)
                      (count . ,count)      ; Number of payments to take
                      (day_of_month . ,day_of_month) ; 1-28 | -1 for last DoM
                      (interval . ,interval)         ; Defaults to 1
                      ;; Should be an assoc list itself
                      (metadata . ,metadata)
                      (month . ,month)      ; Lowcercase name ame of month
                      (name . ,name)        ; Description of subscription
                      (payment_reference . ,payment_reference)
                      ;; If ommited: set to next_possible_charge_date.
                      (start_date . ,start_date)))))))))

(define* (update-subscription id idempotency-key #:key amount app_fee metadata
                              name payment_reference)
  (invalidate-cache)
  (gocardless-put
   (string-append "/subscriptions/" id)
   (put-headers (access-token) idempotency-key)
   (let ((params (filter (match-lambda
                           ((n . #f) #f)
                           ((n . v) #t))
                         `((amount . ,amount)
                           (app_fee . ,app_fee)
                           (metadata . ,metadata)
                           (name . ,name)
                           (payment_reference . ,payment_reference)))))
     (if (null? params)
         (throw 'gocardless-syntax
                "Update subscription should contain a value to update.")
         (scm->json-string
          `((subscriptions
             . ,params)))))))

(define (cancel-subscription id)
  (invalidate-cache)
  (gocardless-post
   (string-join `("/subscriptions" ,id "actions" "cancel") "/")
   (post-headers (access-token))))

;;;;; Mandates

(define* (list-mandates #:key after before created_at> created_at>=
                        created_at< created_at<= (limit 50)
                        creditor customer customer_bank_account reference
                        status)
  (gocardless-get "/mandates"
                  (args->params (param-list "creditor" "customer"
                                            "customer_bank_account"
                                            "reference" "status")
                                after before created_at> created_at>=
                                created_at< created_at<= limit creditor
                                customer customer_bank_account reference
                                status)))

(define (get-mandate id)
  (gocardless-get (string-append "/mandates/" id)))

(define (reinstate-mandate id)
  (gocardless-post (string-join `("/mandates" ,id "actions" "reinstate") "/")
                   (post-headers (access-token))))

;;;;; Payments

(define* (list-payments #:key after before created_at> created_at>=
                         created_at< created_at<= creditor currency customer
                         (limit 50) mandate status subscription)
  (gocardless-get "/payments"
                  (args->params (param-list "creditor" "currency" "customer"
                                            "mandate" "status" "subscription")
                                after before created_at> created_at>=
                                created_at< created_at<= limit creditor currency
                                customer mandate status subscription)))

(define (get-payment id)
  (gocardless-get (string-append "/payments/" id)))

;;;; Accessors

(define (access htable . path)
  (let lp ((path path)
           (result htable))
    (match path
      (() result)
      ((first . rest)
       (when (not (hash-table? result))
         (throw 'gocardless "End of response but path not empty" path))
       (lp rest
           (hash-ref result (symbol->string first)))))))

;;;;; meta

(define (cursor-before scm)
  (access scm 'meta 'cursors 'before))

(define (cursor-after scm)
  (access scm 'meta 'cursors 'after))

(define (cursor-limit scm)
  (access scm 'meta 'limit))

;;;;; customers

(define (customers scm)
  (hash-ref scm "customers"))

(define (customer-id customer)
  (hash-ref customer "id"))

(define (customer-created_at customer)
  (hash-ref customer "created_at"))

(define (customer-email customer)
  (hash-ref customer "email"))

(define (customer-given_name customer)
  (hash-ref customer "given_name"))

(define (customer-family_name customer)
  (hash-ref customer "family_name"))

(define (customer-address_line1 customer)
  (hash-ref customer "address_line1"))

(define (customer-address_line2 customer)
  (hash-ref customer "address_line2"))

(define (customer-address_line3 customer)
  (hash-ref customer "address_line3"))

(define (customer-city customer)
  (hash-ref customer "city"))

(define (customer-region customer)
  (hash-ref customer "region"))

(define (customer-postal_code customer)
  (hash-ref customer "postal_code"))

(define (customer-country_code customer)
  (hash-ref customer "country_code"))

(define (customer-language customer)
  (hash-ref customer "language"))

(define (customer-metadata.URN customer)
  (access customer 'metadata 'custom_reference))

;;;;; events

(define (events scm)
  (access scm 'events))

(define (event-id event)
  (access event 'id))

(define (event-details event)
  (access event 'details))

(define (event-detail-description event)
  (access event 'details 'description))

(define (event-detail-scheme event)
  (access event 'details 'scheme))

(define (event-detail-reason_code event)
  (access event 'details 'reason_code))

(define (event-detail-cause event)
  (access event 'details 'cause))

(define (event-detail-origin event)
  (access event 'details 'origin))

(define (event-created_at event)
  (access event 'created_at))

(define (event-metadata event)
  (access event 'metadata))

(define (event-action event)
  (access event 'action))

(define (event-links event)
  (access event 'links))

(define (event-resource_type event)
  (access event 'resource_type))

;;;;; Linked

(define (linked scm)
  (access scm 'linked))

;;;;; subscriptions

(define (subscriptions scm)
  (access scm 'subscriptions))

(define (subscription-id subscription)
  (access subscription 'id))

(define (subscription-created_at subscription)
  (access subscription 'created_at))

(define (subscription-amount subscription)
  (access subscription 'amount))

(define (subscription-currency subscription)
  (access subscription 'currency))

(define (subscription-status subscription)
  (access subscription 'status))

(define (subscription-name subscription)
  (access subscription 'name))

(define (subscription-start_date subscription)
  (access subscription 'start_date))

(define (subscription-end_date subscription)
  (access subscription 'end_date))

(define (subscription-interval subscription)
  (access subscription 'interval))

(define (subscription-interval_unit subscription)
  (access subscription 'interval_unit))

(define (subscription-day_of_month subscription)
  (access subscription 'day_of_month))

(define (subscription-month subscription)
  (access subscription 'month))

(define (subscription-payment_reference subscription)
  (access subscription 'payment_reference))

(define (subscription-upcoming_payments subscription)
  (access subscription 'upcoming_payments))

(define (subscription-metadata subscription)
  (access subscription 'metadata))

(define (subscription-links subscription)
  (access subscription 'links))

(define (subscription-links-mandate subscription)
  (access subscription 'links 'mandate))

;;;;; mandates

(define (mandates scm)
  (access scm 'mandates))

(define (mandate-id mandate)
  (access mandate 'id))

(define (mandate-created_at mandate)
  (access mandate 'created_at))

(define (mandate-reference mandate)
  (access mandate 'reference))

(define (mandate-status mandate)
  (access mandate 'status))

(define (mandate-scheme mandate)
  (access mandate 'scheme))

(define (mandate-next_possible_charge_date mandate)
  (access mandate 'next_possible_charge_date))

(define (mandate-metadata mandate)
  (access mandate 'metadata))

(define (mandate-links mandate)
  (access mandate 'links))

(define (mandate-links-customer mandate)
  (access mandate 'links 'customer))

;;;;; payments

(define (payments scm)
  (access scm 'payments))

(define (payment-id payment)
  (access payment 'id))

(define (payment-amount payment)
  (access payment 'amount))

(define (payment-amount_refunded payment)
  (access payment 'amount_refunded))

(define (payment-charge_date payment)
  (access payment 'charge_date))

(define (payment-created_at payment)
  (access payment 'created_at))

(define (payment-currency payment)
  (access payment 'currency))

(define (payment-description payment)
  (access payment 'description))

(define (payment-metadata payment)
  (access payment 'metadata))

(define (payment-reference payment)
  (access payment 'reference))

(define (payment-status payment)
  (access payment 'status))

(define (payment-links payment)
  (access payment 'links))

(define (payment-links-creditor payment)
  (access payment 'links 'creditor))

(define (payment-links-mandate payment)
  (access payment 'links 'mandate))

(define (payment-links-payout payment)
  (access payment 'links 'payout))

(define (payment-links-subscription payment)
  (access payment 'links 'subscription))

;;;; Helpers

(define (gc:print scm)
  (pretty-print (rsp->assoc scm)))
