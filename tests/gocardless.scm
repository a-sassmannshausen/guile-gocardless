;; gocardless.scm --- tests for gocardless    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2018 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-gocardless.
;;
;; guile-gocardless is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-gocardless is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-gocardless; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;; Unit tests for gocardless.scm.
;;
;; Source-file: gocardless.scm
;;
;;; Code:

(define-module (tests gocardless)
  #:use-module (ice-9 match)
  #:use-module (ice-9 threads)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (web uri)
  #:use-module (web request)
  #:use-module (gocardless)
  #:use-module (gocardless web))


;;;; Tests
(define considerate (make-parameter #t))

(test-begin "gocardless")

(parameterize ((access-token (access-token))
               (considerate #t))

  (define dummy-access-token "Bearer token-here")

  (when (string=? (access-token) dummy-access-token)
    (test-skip "2 Responses")
    (test-skip "Limited List Customers")
    (test-skip "Limited List Mandates")
    (test-skip "Cancel Non-existent Subscription")
    (test-skip "Simple Subscription Creation")
    (test-skip "Idempotency Error")
    (test-skip "Subscription update")
    (test-skip "Subscription Cancellation"))

  (when (considerate)
    (test-skip "Ratelimiting"))

;;;;; Tests for: base-uri
;;;;; Tests for: params->query-string

  (let ((base-uri (@@ (gocardless web) base-uri)))
    (test-equal "Plain URI."
      (base-uri)
      (build-uri (uri-scheme (base-host)) #:host (uri-host (base-host))
                 #:path "" #:query #f))
    (test-equal "Pathed URI."
      (base-uri "/tst/target/foo")
      (build-uri (uri-scheme (base-host))
                 #:host (uri-host (base-host))
                 #:path "/tst/target/foo"
                 #:query #f))
    (test-equal "Parametered URI."
      (base-uri "" '(("limit" . 56)
                     (name . #f)
                     ("foo" . "bar")))
      (build-uri (uri-scheme (base-host))
                 #:host (uri-host (base-host))
                 #:path ""
                 #:query "limit=56&foo=bar"))
    (test-equal "Pathed Parametered URI."
      (base-uri "/tst/target/foo" '(("frob" . "naz")
                                    ("foo" . "bar")))
      (build-uri (uri-scheme (base-host))
                 #:host (uri-host (base-host))
                 #:path "/tst/target/foo"
                 #:query "frob=naz&foo=bar")))

;;;;; Tests for: base-headers

  (let ((base-headers (@@ (gocardless web) base-headers)))
    (test-equal "Base Headers"
      (base-headers (access-token))
      `((authorization
         bearer . ,(second (string-split (access-token) #\space)))
        (connection close) (gocardless-version . "2015-07-06")
        (accept (application/json)))))

;;;;; Tests for: post-headers
  (let ((post-headers (@@ (gocardless web) post-headers)))
    (test-equal "Plain Post"
      (post-headers (access-token))
      `((content-type application/json)
        (authorization
         bearer . ,(second (string-split (access-token) #\space)))
        (connection close) (gocardless-version . "2015-07-06")
        (accept (application/json))))
    (test-equal "Idempotency Post"
      (post-headers (access-token) "foobarbaz")
      `((idempotency-key . "foobarbaz")
        (content-type application/json)
        (authorization
         bearer . ,(second (string-split (access-token) #\space)))
        (connection close) (gocardless-version . "2015-07-06")
        (accept (application/json)))))


;;;;; Tests for: gocardless-get
;;;;; Tests for: response-handler

  (let ((gocardless-get (@@ (gocardless web) gocardless-get)))
    (test-equal "Fresh Cache"
      0 (hash-count identity (gocardless-get "" #:show-cache? #t)))
    (test-error "404 Query" 'gocardless-code (gocardless-get ""))
    (test-equal "2 Responses"
      2 (length (subscriptions
                 (gocardless-get "/subscriptions/" '(("limit" . 2))))))
    (gocardless-get "" #:invalidate-cache? #t)
    (test-equal "Invalidated Cache"
      0 (hash-count identity (gocardless-get "" #:show-cache? #t)))
    (test-assert "Ratelimiting"
      (begin
        (par-for-each (lambda (n)
                        (invalidate-cache)
                        (list-subscriptions #:limit 1))
                      (iota 1001))
        #t)))

;;;;; Tests for: gocardless-post

  (let ((gocardless-post (@@ (gocardless web) gocardless-post)))
    (test-error "404 Query" 'gocardless-code (gocardless-post "")))

;;;;; Tests for: list-customers

  (test-assert "Limited List Customers"
    (< (length (customers (list-customers #:limit 2))) 3))

;;;;; Tests for: list-mandates

  (test-assert "Limited List Mandates"
    (< (length (mandates (list-mandates #:limit 2))) 3))

;;;;; Tests for: create-subscription
;;;;; Tests for: update-subscription
;;;;; Tests for: cancel-subscription

  (test-error "Cancel Non-existent Subscription" 'gocardless-code
              (cancel-subscription "Unlikely-Subscription-ID"))

  (let* ((idemk (object->string (gettimeofday)))
         (tst (if (string=? (access-token) dummy-access-token)
                  #f
                  (create-subscription
                   100 "GBP" "monthly"
                   (mandate-id (first (mandates (list-mandates #:limit 1))))
                   idemk))))
    (test-assert "Simple Subscription Creation" tst)
    (test-error "Idempotency Error" 'gocardless-code
                (create-subscription
                 1100 "GBP" "weekly"
                 (mandate-id (first (mandates (list-mandates #:limit 2))))
                 idemk))
    (test-assert "Subscription update"
      (update-subscription (subscription-id (subscriptions tst))
                           (object->string (gettimeofday))
                           #:amount 1500))
    (test-assert "Subscription Cancellation"
      (cancel-subscription (subscription-id (subscriptions tst))))))

(test-end "gocardless")

(exit (= (test-runner-fail-count (test-runner-current)) 0))
